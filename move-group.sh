#!/usr/bin/env bash

set -euo pipefail
GROUPID=$1
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

while IFS= read -r ID; do
echo ""
echo "-- processing ...${ID} --"
DATA="{\"namespace\":\"$GROUPID\"}"

curl --request PUT \
	--header "$GL_AUTH" \
	--header "Content-Type: application/json" \
	--data "$DATA" \
	"https://gitlab.com/api/v4/projects/$ID/transfer"

echo ""
echo "-- completed --"
echo ""
done < "$DIR/projects.txt"
