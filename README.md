#

## Set your API KEY
I am using a `Personal Token` but you can another method avilable in the docs.

```bash
export GL_TOKEN=xyz
export GL_AUTH="Private-Token: $GL_TOKEN"
```

## Create Project List
In my case I wanted to move all my projects `owned=yes` under our organization. An accidental `Import All` from GitHub landed then under my ownership and in my personal group.

### Sample Query
```bash
curl -v --header "$GL_AUTH" "https://gitlab.com/api/v4/projects?simple=yes&owned=yes&per_page=100&page=1" | jq '.[] | .id' > projects.txt
```

## Find your target group identfier

### By Ownership
```bash
curl -v --header "$GL_AUTH" "https://gitlab.com/api/v4/groups?owned=yes" | jq '.[] | {name,id}'
```

## Run Migration

```bash
$GROUPID=7651483
./move-groups.sh $GROUPID
```
### EXAMPLE -- the Transfer API
The script `move-groups.sh` leverages the Project API to 
transfer `PUT /projects/:id/transfer` your project to a new group.

```bash
DATA="{\"namespace\":\"$GROUPID\"}"
curl -v --request PUT \
	--header "$GL_AUTH" \
	--header "Content-Type: application/json" \
	--data $DATA \
	"https://gitlab.com/api/v4/projects/$ID/transfer"
```


